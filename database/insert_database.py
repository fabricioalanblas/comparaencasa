import mysql.connector
import json

mydb = mysql.connector.connect(
  host= "localhost",
  user= "root",
  port= 3306,
  password= "comparaencasa",
  database= "mydatabase"
)

mycursor = mydb.cursor()

mycursor.execute("CREATE TABLE cars (id INT AUTO_INCREMENT PRIMARY KEY, car_name VARCHAR(255), car_plate VARCHAR(255))")

with open('cars.json') as f:
  data = json.load(f)

for i in data['cars']:
    print(i)
    mycursor.execute("INSERT INTO cars (car_name, car_plate) VALUES (%s, %s)", (i["car_name"],i["car_plate"]))

mydb.commit()
